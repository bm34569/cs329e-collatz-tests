#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_calc, collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):

    def test_calc_1(self):
        s = 2
        i = collatz_calc(s)
        self.assertEqual(i, 2)

    def test_calc_2(self):
        s = 3
        i = collatz_calc(s)
        self.assertEqual(i, 8)

    def test_calc_3(self):
        s = 10
        i = collatz_calc(s)
        self.assertEqual(i, 7)

    def test_calc_4(self):
        s = 45
        i = collatz_calc(s)
        self.assertEqual(i, 17)

    # ----
    # read ---- Testing whether or not it can read a string and turn it into 2 numbers
    # ----

    def test_read_1(self):
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1)
        self.assertEqual(j, 10)

    def test_read_2(self):
        s = "1000 2100\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  1000)
        self.assertEqual(j, 2100)

    def test_read_3(self):
        s = "341 234\n"
        i, j = collatz_read(s)
        self.assertEqual(i,  341)
        self.assertEqual(j, 234)

    # ----
    # eval ---- This is the actual collatz function test
    # ----

    def test_eval_1(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    def test_eval_2(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)

    def test_eval_3(self):
        v = collatz_eval(1, 1000000)
        self.assertEqual(v, 525)

    def test_eval_4(self):
        v = collatz_eval(152515, 600000)
        self.assertEqual(v, 470)

    def test_eval_5(self):
        v = collatz_eval(1, 600900)
        self.assertEqual(v, 470)

    def test_eval_6(self):
        v = collatz_eval(1000, 600900)
        self.assertEqual(v, 470)
    # -----
    # -----
    # print ---- testing whether it prints or not
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 1, 32, 100)
        self.assertEqual(w.getvalue(), "1 32 100\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 43, 200, 30)
        self.assertEqual(w.getvalue(), "43 200 30\n")

    def test_print_4(self):
        w = StringIO()
        collatz_print(w, 81, 110, 120)
        self.assertEqual(w.getvalue(), "81 110 120\n")

    # -----
    # solve ---- With input,
    # -----

    def test_solve_1(self):

        # r is the input, what's being read
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")

        # w is the output, what is being written
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
        # change the above values to be the answers

    def test_solve_2(self):

        # r is the input, what's being read
        r = StringIO("2 34\n3 50\n200 400\n")

        # w is the output, what is being written
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2 34 112\n3 50 112\n200 400 144\n")
        # change the above values to be the answers

    def test_solve_3(self):

        # r is the input, what's being read
        r = StringIO("2 15\n400 800\n")

        # w is the output, what is being written
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "2 15 20\n400 800 171\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
