#!/usr/bin/env python3

# -------------------------------
# projects/collatz/TestCollatz.py
# Copyright (C) 2016
# Glenn P. Downing
# -------------------------------

# https://docs.python.org/3.4/reference/simple_stmts.html#grammar-token-assert_stmt

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------


class TestCollatz (TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        # going to want generators in the future for efficiency..
        s = "1 10\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 10)

    def test_read_1(self):
        # going to want generators in the future for efficiency..
        s = "5 20\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 5)
        self.assertEqual(j, 20)

    def test_read_2(self):
        # going to want generators in the future for efficiency..
        s = "1 2\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 1)
        self.assertEqual(j, 2)

    def test_read_3(self):
        # going to want generators in the future for efficiency..
        s = "9 100\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 9)
        self.assertEqual(j, 100)

    # ----
    # eval
    # ----

    def test_eval(self):
        v = collatz_eval(1, 10)
        self.assertEqual(v, 20)

    # --------
    # My tests
    # --------
    def test_eval_1(self):
        v = collatz_eval(100, 200)
        self.assertEqual(v, 125)

    def test_eval_2(self):
        v = collatz_eval(201, 210)
        self.assertEqual(v, 89)

    def test_eval_3(self):
        v = collatz_eval(900, 1000)
        self.assertEqual(v, 174)

    # ----------------
    # My Corner Cases
    # ----------------

    def test_eval_4(self):
        v = collatz_eval(1, 1)
        self.assertEqual(v, 1)  # should be one

    def test_eval_5(self):
        v = collatz_eval(1, 2)
        self.assertEqual(v, 2)

    def test_eval_6(self):
        v = collatz_eval(10, 1)
        self.assertEqual(v, 20)

    # ------------
    # Failure Case
    # ------------
    def test_eval_7(self):
        v = collatz_eval(200, 100)
        self.assertEqual(v, 125)

    def test_eval_8(self):
        v = collatz_eval(210, 201)
        self.assertEqual(v, 89)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 1, 10, 20)
        self.assertEqual(w.getvalue(), "1 10 20\n")
    # --------
    # My tests
    # --------

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 72, 348, 144)
        self.assertEqual(w.getvalue(), "72 348 144\n")

    def test_print_2(self):
        w = StringIO()
        collatz_print(w, 34, 347, 144)
        self.assertEqual(w.getvalue(), "34 347 144\n")

    def test_print_3(self):
        w = StringIO()
        collatz_print(w, 31, 19, 112)
        self.assertEqual(w.getvalue(), "31 19 112\n")
    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("1 10\n100 200\n201 210\n900 1000\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "1 10 20\n100 200 125\n201 210 89\n900 1000 174\n")
    # --------
    # My tests
    # --------

    def test_solve_1(self):
        r = StringIO("52 14\n36 359\n20 198\n61 188\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "52 14 112\n36 359 144\n20 198 125\n61 188 125\n")

    def test_solve_2(self):
        r = StringIO("75 172\n79 349\n15 42\n36 376\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "75 172 125\n79 349 144\n15 42 112\n36 376 144\n")

    def test_solve_3(self):
        r = StringIO("47 314\n9 42\n33 190\n21 129\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(
            w.getvalue(), "47 314 131\n9 42 112\n33 190 125\n21 129 122\n")

# ----
# main
# ----


if __name__ == "__main__":
    main()

""" #pragma: no cover
% coverage run --branch TestCollatz.py >  TestCollatz.out 2>&1


% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK


% coverage report -m                   >> TestCollatz.out



% cat TestCollatz.out
.......
----------------------------------------------------------------------
Ran 7 tests in 0.000s
OK
Name             Stmts   Miss Branch BrPart  Cover   Missing
------------------------------------------------------------
Collatz.py          12      0      2      0   100%
TestCollatz.py      32      0      0      0   100%
------------------------------------------------------------
TOTAL               44      0      2      0   100%
"""
